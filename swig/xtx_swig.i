/* -*- c++ -*- */

#define XTX_API

%include "gnuradio.i"			// the common stuff

//load generated python docstrings
%include "xtx_swig_doc.i"

%{
#include "xtx/divisor.h"
%}


%include "xtx/divisor.h"
GR_SWIG_BLOCK_MAGIC2(xtx, divisor);
