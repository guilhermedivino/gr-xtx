INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_XTX xtx)

FIND_PATH(
    XTX_INCLUDE_DIRS
    NAMES xtx/api.h
    HINTS $ENV{XTX_DIR}/include
        ${PC_XTX_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    XTX_LIBRARIES
    NAMES gnuradio-xtx
    HINTS $ENV{XTX_DIR}/lib
        ${PC_XTX_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(XTX DEFAULT_MSG XTX_LIBRARIES XTX_INCLUDE_DIRS)
MARK_AS_ADVANCED(XTX_LIBRARIES XTX_INCLUDE_DIRS)

